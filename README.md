This was an Ansible role to install the [Drupal
Console](https://drupalconsole.com/) using [the
phar](https://drupalconsole.com/docs/en/getting/launcher), however this is [not
working](https://github.com/hechoendrupal/drupal-console/issues/4262) and also:

> in future versions this installer will be removed

So don't use this role!
