---
- name: Install or upgrade the Drupal Console
  block:

    - name: Check if Drupal is installed
      shell: which drupal || echo absent
      check_mode: false
      register: drupal_path_check
      changed_when: '"drupal" not in drupal_path_check.stdout'

    - name: Check the latest version of Drupal
      uri:
        url: https://github.com/hechoendrupal/drupal-console/releases/latest
        method: HEAD
        status_code: 302
        follow_redirects: none
      check_mode: false
      register: drupal_headers

    - name: Debug Drupal URI
      debug:
        msg:
          - "Location: {{ drupal_headers.location }}"
          - "Path: {{ drupal_headers.location | urlsplit('path') }}"
          - "Version: {{ drupal_headers.location | urlsplit('path') | basename | trim }}"
        verbosity: 1

    - name: Set a variable for the latest version available
      set_fact:
        drupal_version: "{{ drupal_headers.location | urlsplit('path') | basename | trim }}"

    - name: Checks when Drupal is installed
      block:

        - name: Check installed Drupal version
          command: drupal --no-ansi --version
          check_mode: false
          changed_when: false
          register: drupal_installed_check

        - name: Print installed Drupal version
          debug:
            msg: "{{ drupal_installed_check.stdout }} is installed"
            verbosity: 1

        - name: Set a variable for the installed version of Drupal
          set_fact:
            drupal_installed: "{{ drupal_installed_check.stdout | replace('Drupal Console Launcher', '') | trim }}"

        - name: Set a fact if a newer version is available
          set_fact:
            drupal_upgrade: true
          when:
            - ( drupal_installed is defined ) and ( drupal_installed | length > 0 )
            - ( drupal_version is defined ) and ( drupal_version | length > 0 )
            - drupal_installed != drupal_version

      when: ( drupal_path_check is defined ) and ( "drupal" in drupal_path_check.stdout )

    - name: Install or upgrade the Drupal Console
      block:

        - name: Download the Drupal Console
          block:

            - name: Download the Drupal Console
              get_url:
                url: https://drupalconsole.com/installer
                dest: /usr/local/src/drupal.phar
                force: true
              register: drupal_console_phar

          rescue:

            - name: Download failed
              debug:
                msg:
                  - "The download of the Drupal Console version {{ drupal_version }} from https://drupalconsole.com/installer"
                  - "failed with the HTTP error code {{ drupal_console_phar.status_code }}."

        - name: Install the Drupal Console
          copy:
            src: /usr/local/src/drupal.phar
            dest: /usr/local/bin/drupal
            mode: 0755
            owner: root
            group: root
            remote_src: true
          when: drupal_console_phar.changed

      when: ( "drupal" not in drupal_path_check.stdout ) or ( drupal_upgrade is defined and drupal_upgrade )

  tags:
    - drupal
...
